
## js-utils

```
npm install @yoronsoft/js-utils
```

### 使用说明

#### utils 工具
```typescript jsx
import {utils} from '@yoronsoft/js-utils'

utils.isArray(data)
// => boolean
utils.isJson(data)
// => boolean
utils.isNumber(data)
// => boolean
utils.isInteger(data)
// => boolean
utils.isAmount(data, params)
// => boolean
utils.isStrFirst(data, params)
// => boolean
utils.isStrLast(data, params)
// => boolean

utils.toMD5(data)
// => string
utils.toEncrypt(data, key, iv)
// => string
utils.toDecrypt(data, key, iv)
// => string
utils.toStrFirst(data, length)
// => string
utils.toStrLast(data.length)
// => string
utils.toStrCut(data, params)
// => string
utils.toNumber(data)
// => number
utils.toAmount(data, params)
// => string
utils.toAmountConvert(data)
// => string
utils.toScientificCount(data)
// => string
utils.toRandom(count)
// => string
utils.toShuffle<T>(arr)
// => T[]
utils.toCopyDeepJson<T>(data)
// => T
utils.toSearchByJson(data)
// => string
utils.toJsonBySearch<T>(data)
// => T

utils.replaceStrByJson(data, json)
//  => string
utils.replaceOrSpliceToUrlByJson(url, json)
//  => string

utils.formatDate(data, params)
//  => string
utils.formatDateTimezoneOffset(data, params)
//  => string
utils.formatDateToJson(data, params)
//  => FormatDataResult

utils.contrastJson<T>(jsonOne, jsonTwo)
//  => boolean
utils.contrastJsonToDiff<T>(oldJson, newJson)
//  => T
utils.contrastArrayToDiff<T>(oldJson, newJson, contrastFields)
//  => { oldArrDiff: T[], newArrDiff: T[] }
utils.contrastVersion(serverVersion, localVersion, serverBuild, localBuild)
//  => boolean
```

#### XmlHttpRequest 数据请求

```typescript jsx
//  header 类型
import {XmlHttpRequest} from "@yoronsoft/js-utils";

XmlHttpRequest.header

//  contentType 类型
XmlHttpRequest.contentType

//  responseType 类型
XmlHttpRequest.responseType

//  method 类型
XmlHttpRequest.method

//  请求数据
const http = new XmlHttpRequest();
//  GET 方法
http.get(url, body)
//  POST 方法
http.post(url, body)
//  PUT 方法
http.put(url, body)
//  DELETE 方法
http.delete(url, body)

//  设置异步，默认为异步
http.setAsync(data);

//  设置 header
http.setHeader(name, value);

//  设置 HeaderBasic
http.setHeaderBasic(value);

//  设置 HeaderBasicBy
http.setHeaderBasicBy(account, password);

//  设置 HeaderBearer
http.setHeaderBearer(value);

//  设置 ResponseType
http.setResponseType(data);

//  设置 过期时间
http.setTimeout(name, value);

```

#### redux

```typescript jsx
import {redux} from "@yoronsoft/js-utils";

redux.all();
//  => any
redux.get<T>(name)
//  => T
redux.create<T>(name, params, isInit)
//  => T
redux.update<T>(name, params)
//  => T
redux.remove(name)
//  => void
redux.removeAndInit(name)
//  => void
```
