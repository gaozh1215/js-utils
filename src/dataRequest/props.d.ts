import {LoggerTypeData} from "../logger";

export interface IDataRequest {
    /**
     * 数据请求类型
     */
    readonly get: <T>(url: string, body?: object) => Promise<T>
    readonly post: <T>(url: string, body?: object) => Promise<T>
    readonly delete: <T>(url: string, body?: object) => Promise<T>
    readonly put: <T>(url: string, body?: object) => Promise<T>

    //  设置 请求头
    readonly setHeader: (name: string, value: string) => void
    //  设置 Bearer
    readonly setHeaderBearer: (value: string) => void
    //  设置 Basic
    readonly setHeaderBasic: (value: string) => void
    //  设置 BasicBy
    readonly setHeaderBasicBy: (account: string, password: string) => void
    //  设置 Async
    readonly setAsync: (data: boolean) => void
    //  设置 过期时间
    readonly setTimeout: (data: number) => void
    //  设置 返回类型
    readonly setResponseType: (data: XMLHttpRequestResponseType) => void

    //  设置 请求
    readonly setRequest: (data: any) => void

    //  中断请求
    readonly abort: () => void
    readonly onabort: (callback: () => void) => void
}


/**
 * default 静态返回
 */
export interface IDataRequestDefaultStatic {
    new(params?: DataRequestParams): IDataRequest

    readonly type: RequestType
    readonly header: RequestHeader
    readonly contentType: RequestContentType
    readonly responseType: RequestResponseType
    readonly method: RequestMethod
    readonly error: RequestError
}


export type DataRequestParams = {
    logger?: LoggerTypeData
    type?: typeof RequestType
}

export type RequestType = {
    readonly xml: 'xml'
    readonly fetch: 'fetch'
    readonly fetchStream: 'fetchStream'
}

export type RequestMethod = {
    readonly get: 'GET'
    readonly post: 'POST'
    readonly delete: 'DELETE'
    readonly put: 'PUT'
}

export type RequestMethodData = 'GET' | 'POST' | 'DELETE' | 'PUT';

export type RequestContentType = {
    readonly applicationJson: 'application/json',
    readonly applicationWWW: 'application/x-www-form-urlencoded',
    readonly textHtml: 'text/html',
    readonly fromData: 'multipart/form-data',
}

export type RequestHeader = {
    readonly contentType: 'Content-Type'
    readonly authorization: 'Authorization'
}

export type RequestResponseType = {
    empty: XMLHttpRequestResponseType,
    json: XMLHttpRequestResponseType,
    text: XMLHttpRequestResponseType,
    document: XMLHttpRequestResponseType,
    blob: XMLHttpRequestResponseType,
    arraybuffer: XMLHttpRequestResponseType,
}

export type RequestError = {
    //  数据错误
    err_400: { code: 400, msg: string }
    //  未授权
    err_401: { code: 401, msg: string }
    //  页面未找到
    err_404: { code: 404, msg: string }
    //  不允许的方法
    err_405: { code: 405, msg: string }
    //  不可接受
    err_406: { code: 406, msg: string }
    //  需要代理验证
    err_407: { code: 407, msg: string }
    //  请求超时
    err_408: { code: 408, msg: string }
    //  冲突
    err_409: { code: 409, msg: string }
    //  预处理失败
    err_412: { code: 412, msg: string }
    //  内容太长
    err_413: { code: 413, msg: string }
    //  uri太长
    err_414: { code: 414, msg: string }
    //  不支持的媒体类型
    err_415: { code: 415, msg: string }
    //  范围不符合要求
    err_416: { code: 416, msg: string }
    //  预期失败
    err_417: { code: 417, msg: string }
    //  内部服务器错误
    err_500: { code: 500, msg: string }
    //  未实施
    err_501: { code: 501, msg: string }
    //  错误网关
    err_502: { code: 502, msg: string }
    //  服务不可用
    err_503: { code: 503, msg: string }
    //  网关超时
    err_504: { code: 504, msg: string }
    //  HTTP版本不受支持
    err_505: { code: 505, msg: string }
    //  存储不足
    err_507: { code: 507, msg: string }
    //  检测到有循环
    err_508: { code: 508, msg: string }
    //  未扩展
    err_510: { code: 510, msg: string }
    //  需要网络身份
    err_511: { code: 511, msg: string }
    //  未知错误
    err_10000: { code: 10000, msg: string }
}

export type RequestParams = {
    method: typeof RequestMethod
    url: string
    body: object | undefined
    logger?: LoggerTypeData
    headers: RequestParamsHeaders[]
    request?: any
    async?: boolean
    timeout?: number
    responseType?: XMLHttpRequestResponseType
}

export type RequestParamsHeaders = {
    name: string
    value: string
}

export type RequestCallback = {
    //  返回对象
    object?: (xml: any) => void
    //  请求终止
    onabort?: () => void
    //  流式返回，仅限于类型是fetchStream
    stream?: (data: T) => void
}
