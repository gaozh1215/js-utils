import {DataRequestDefault} from '../config';
import {RequestCallback, RequestParams} from "../props";
import utils from "../../utils";
import {Logger} from "../../logger";


const RequestData = <T>(params: RequestParams, callback?: RequestCallback): Promise<T> => {
    return new Promise((resolve, reject) => {
        //  logger
        const logger = new Logger('XmlHttpRequest', params.logger);
        callback = callback || {};
        //  设置请求头
        if (typeof params.headers === "undefined") params.headers = [];
        //  判断是否有请求类型contentType
        if (params.headers.findIndex(m => m.name === DataRequestDefault.header.contentType) < 0) params.headers.push({
            name: DataRequestDefault.header.contentType,
            value: DataRequestDefault.contentType.applicationJson
        });
        //  初始化 body
        if (typeof params.body !== 'object') params.body = {};
        //  初始化 是否异步
        params.async = typeof params.async === "boolean" ? params.async : true;
        //  初始化 时间
        params.timeout = typeof params.timeout === "number" ? params.timeout : 0;
        //  初始化 返回类型
        params.responseType = params.responseType ?? DataRequestDefault.responseType.json;


        let body: object | string | undefined, url: string;
        //  请求类型
        if (params.method === DataRequestDefault.method.get) {
            url = utils.replaceOrSpliceToUrlByJson(params.url, params.body);
            body = undefined;
        } else {
            url = utils.replaceStrByJson(params.url, params.body);
            const contentType = params.headers.find(m => m.name === DataRequestDefault.header.contentType);
            if (contentType) {
                switch (contentType.value) {
                    case DataRequestDefault.contentType.applicationJson:
                        body = JSON.stringify(params.body);
                        break;
                    case DataRequestDefault.contentType.applicationWWW:
                        body = utils.toSearchByJson(params.body);
                        break;
                    case DataRequestDefault.contentType.textHtml:
                        break;
                    case DataRequestDefault.contentType.fromData:
                        body = params.body;
                        break;
                    default:
                        break;
                }
            }
        }

        logger.log(`url:${url}`, `body:${body}`, `params:${params}`);

        const RequestXml = params.request ?? XMLHttpRequest;
        const xhr = new RequestXml();
        //  返回Xml对象
        if (callback && typeof callback.object === "function") callback.object(xhr);
        //  初始化配置
        xhr.open(params.method, url, params.async);
        //  设置header
        params.headers.map(item => xhr.setRequestHeader(item.name, item.value));
        //  设置期望的返回数据类型
        xhr.responseType = params.responseType;
        //  设置 超时 方法
        let timeout: any, isEnd = false;
        //  请求开始
        xhr.onloadstart = () => {
            //  判断超过设置时间
            if (Number(params.timeout ?? 0) > 0) {
                timeout = setTimeout(() => {
                    isEnd = true;
                    xhr.abort();
                    logger.warn('ontimeout', `url:${url}`, `body:${body}`);
                    reject(DataRequestDefault.error.err_408);
                }, params.timeout);
            }
        }
        //  请求结束
        xhr.onloadend = () => {
            logger.log('onloadend');
        }
        //  请求超时
        xhr.ontimeout = () => {
            logger.log('ontimeout');
            reject(DataRequestDefault.error.err_408);
        }
        //  请求被终止
        xhr.onabort = () => {
            logger.log('onabort');
            if (callback && typeof callback.onabort === "function") callback.onabort();
        }
        //  请求出错
        xhr.onerror = () => {
            logger.err('ontimeout', `url:${url}`, `body:${body}`);
            reject(DataRequestDefault.error.err_10000);
        }
        //  请求完成
        xhr.onload = () => {
            //  清除过期时间
            if (timeout) clearTimeout(timeout);
            //  是否结束
            if (isEnd) return;
            const {status, readyState, responseType} = xhr;
            if (readyState === 4) {
                //  获取 response
                let result = xhr.response;
                if (result === null && xhr['_response']) result = xhr['_response'];
                //  获取 responseText
                if (typeof result === "undefined") {
                    result = xhr.responseText;
                    if (xhr.responseText === null) result = xhr['_responseText'];
                }
                //  如果等于null
                if (result === null) result = undefined;
                //  成功状态
                if (status === 200) {
                    switch (responseType) {
                        case DataRequestDefault.responseType.json:
                            let jsonData = result;
                            if (typeof result === 'string') {
                                if (utils.isJson(jsonData)) jsonData = JSON.parse(jsonData)
                            }
                            resolve(jsonData);
                            break;
                        case DataRequestDefault.responseType.text:
                            resolve(result);
                            break;
                        default:
                            break;
                    }
                } else {
                    const err = DataRequestDefault.error[`err_${status}`];
                    //  初始化 errors
                    const errorList: string[] = [];
                    if (typeof result !== "undefined") {
                        //  判断错误信息
                        switch (responseType) {
                            case DataRequestDefault.responseType.json:
                                let jsonData = result;
                                if (typeof result === 'string' && utils.isJson(result)) jsonData = JSON.parse(result);
                                const errors = jsonData['errors'];
                                if (typeof errors === 'object') {
                                    for (let name in errors) {
                                        const list = errors[name];
                                        if (list && list.length > 0) {
                                            list.map((item: string) => errorList.push(item))
                                        }
                                    }
                                }
                                break;
                            case DataRequestDefault.responseType.text:
                                errorList.push(result);
                                break;
                            default:
                                break;
                        }
                    }
                    err.errors = errorList;
                    //  未知错误
                    if (typeof err === "undefined") {
                        logger.err(`未知status：${status}`);
                        reject(DataRequestDefault.error.err_10000);
                    } else {
                        reject(err);
                    }
                }
            }
        }
        //  设置超时时间,0表示永不超时
        xhr.timeout = params.timeout;
        //  发送请求
        xhr.send(body)
    })
}

export default RequestData
