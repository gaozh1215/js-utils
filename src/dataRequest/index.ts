import {DataRequestParams, IDataRequest, RequestParams} from "./props";
import {DataRequestDefault} from "./config";
import {Logger} from "../logger";
import cryptoTools from "../crypto";
import XmlHttpRequest from "./xmlHttpRequest";

class DataRequest extends DataRequestDefault implements IDataRequest {
    private readonly params: RequestParams
    private readonly type: string
    private object: any

    constructor(params?: DataRequestParams) {
        super();
        params = params || {};
        this.type = params.type ?? DataRequestDefault.type.xml;
        this.params = {
            headers: [],
            body: undefined, method: undefined, url: "",
            logger: params.logger ?? Logger.type.release
        };
    }

    /**
     * 中断请求
     */
    abort(): void {
        if (this.object) this.object.abort();
    }

    /**
     * 中断请求回调
     */
    onabort(callback: () => void): void {
        callback();
    }


    delete<T>(url: string, body?: object): Promise<T> {
        this.params.url = url;
        if (body) this.params.body = body;
        this.params.method = DataRequestDefault.method.delete;
        return XmlHttpRequest<T>(this.params, {
            object: (data) => this.object = data,
            onabort: () => this.onabort
        })
    }

    get<T>(url: string, body?: object): Promise<T> {
        this.params.url = url;
        if (body) this.params.body = body;
        this.params.method = DataRequestDefault.method.get;
        return XmlHttpRequest<T>(this.params, {
            object: (data) => this.object = data,
            onabort: () => this.onabort
        })

    }

    post<T>(url: string, body?: object): Promise<T> {
        this.params.url = url;
        if (body) this.params.body = body;
        this.params.method = DataRequestDefault.method.post;
        return XmlHttpRequest<T>(this.params, {
            object: (data) => this.object = data,
            onabort: () => this.onabort
        })
    }

    put<T>(url: string, body?: object): Promise<T> {
        this.params.url = url;
        if (body) this.params.body = body;
        this.params.method = DataRequestDefault.method.put;
        return XmlHttpRequest<T>(this.params, {
            object: (data) => this.object = data,
            onabort: () => this.onabort
        })
    }

    setAsync(data: boolean): void {
        this.params.async = data;
    }

    setHeader(name: string, value: string): void {
        this.params.headers.push({name, value});
    }

    setHeaderBasic(value: string): void {
        this.params.headers.push({name: 'Authorization', value: `Basic ${value}`});
    }

    setHeaderBasicBy(account: string, password: string): void {
        const value = cryptoTools.base64Encrypt(`${account}:${password}`);
        this.setHeaderBasic(value);
    }

    setHeaderBearer(value: string): void {
        this.params.headers.push({name: 'Authorization', value: `Bearer ${value}`})
    }

    setResponseType(data: XMLHttpRequestResponseType): void {
        this.params.responseType = data;
    }

    setTimeout(data: number): void {
        this.params.timeout = data;
    }

    setRequest(data: any): void {
        this.params.request = data;
    }
}

export default DataRequest
export {
    DataRequestDefault
}
export type {
    DataRequestParams, IDataRequest, RequestParams
}
