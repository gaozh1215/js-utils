import {
    RequestContentType,
    RequestError,
    RequestHeader,
    RequestMethod,
    RequestResponseType,
    RequestType
} from "./props";

export class DataRequestDefault {
    static readonly type: RequestType = {
        xml: "xml",
        fetch: "fetch",
        fetchStream: "fetchStream"
    }
    static readonly header: RequestHeader = {
        contentType: 'Content-Type',
        authorization: 'Authorization'
    }
    static readonly contentType: RequestContentType = {
        applicationJson: 'application/json',
        applicationWWW: 'application/x-www-form-urlencoded',
        textHtml: 'text/html',
        fromData: 'multipart/form-data',
    }
    static readonly responseType: RequestResponseType = {
        empty: '',
        json: 'json',
        text: 'text',
        document: 'document',
        blob: 'blob',
        arraybuffer: 'arraybuffer',
    }
    static readonly method: RequestMethod = {
        get: 'GET',
        post: 'POST',
        delete: 'DELETE',
        put: 'PUT',
    }
    static readonly error: RequestError | any = {
        err_400: {code: 400, msg: "400 Data Error"},
        err_401: {code: 401, msg: "401 Unauthorized"},
        err_404: {code: 404, msg: "404 Not Found"},
        err_405: {code: 405, msg: "405 Method Not Allowed"},
        err_406: {code: 406, msg: "406 Not Acceptable"},
        err_407: {code: 407, msg: "407 Proxy Authentication Required"},
        err_408: {code: 408, msg: "408 Request Timeout"},
        err_409: {code: 409, msg: "409 Conflict"},
        err_412: {code: 412, msg: "412 Precondition Failed"},
        err_413: {code: 413, msg: "413 Content Too Large"},
        err_414: {code: 414, msg: "414 URI Too Long"},
        err_415: {code: 415, msg: "415 Unsupported Media Type"},
        err_416: {code: 416, msg: "416 Range Not Satisfiable"},
        err_417: {code: 417, msg: "417 Expectation Failed"},
        err_500: {code: 500, msg: "500 Internal Server Error"},
        err_501: {code: 501, msg: "501 Not Implemented"},
        err_502: {code: 502, msg: "502 Bad Gateway"},
        err_503: {code: 503, msg: "503 Service Unavailable"},
        err_504: {code: 504, msg: "504 Gateway Timeout"},
        err_505: {code: 505, msg: "505 HTTP Version Not Supported"},
        err_507: {code: 507, msg: "507 Insufficient Storage"},
        err_508: {code: 508, msg: "508 Loop Detected"},
        err_510: {code: 510, msg: "510 Not Extended"},
        err_511: {code: 511, msg: "511 Network Authentication Required"},
        err_10000: {code: 10000, msg: "Unknown error"}
    }
}

