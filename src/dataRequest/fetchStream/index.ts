import {RequestCallback, RequestParams} from "../props";


const RequestData = (params: RequestParams, callback?: RequestCallback): void => {
    const streamRequest = async () => {
        const init: RequestInit = {
            body: JSON.stringify(params.body),
            method: params.method,
        }
        const response = await fetch(params.url, init);
        const reader: any = response.body ? response.body.getReader() : undefined;
        if (reader) {
            while (true) {
                const {done, value} = await reader.read();
                if (done) break;
                const dataText = new TextDecoder().decode(value);
                const list = dataText.split('\n\n').filter(Boolean) as string[];
                if (list.length) list.map(item => {
                    const dataStr = item.split("data:")[1];
                    const result = JSON.parse(dataStr);
                    if (callback && typeof callback.stream === "function") callback.stream(result);
                })
            }
        }
    }
    streamRequest().then()
}

export default RequestData
