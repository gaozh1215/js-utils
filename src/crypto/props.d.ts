export interface ICrypto {
    readonly mdt5: (data: string) => string
    readonly base64Encrypt: (data: string) => string
    readonly base64Decrypt: (data: string) => string
}
