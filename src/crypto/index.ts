import {ICrypto} from "./props";
import {enc, MD5} from 'crypto-js';

class Crypto implements ICrypto {
    base64Decrypt(data: string): string {
        try {
            return enc.Base64.parse(data).toString(enc.Utf8);
        } catch (err) {
            return ''
        }
    }

    base64Encrypt(data: string): string {
        return enc.Base64.stringify(enc.Utf8.parse(data));
    }

    mdt5(data: string): string {
        return MD5(data).toString();
    }
}

const crypto: ICrypto = new Crypto();
export default crypto

