import redux from '../redux';

interface ILoggerClass {
    new(name?: string, type?: LoggerTypeData): Logger
}

interface ILogger {
    readonly log: (...obj: any) => void
    readonly warn: (...obj: any) => void
    readonly err: (...obj: any) => void
    readonly timeStart: (name: string) => void
    readonly timeEnd: (name: string) => void
    readonly timeLog: (name: string, ...obj: any) => void
}

type LoggerType = {
    readonly release: 'release'
    readonly debug: 'debug'
}

type LoggerTypeData = 'release' | 'debug'


/**
 * Logger
 */
class Logger implements ILogger {
    private readonly name: string
    private readonly type: LoggerTypeData
    private readonly defaultName: string

    constructor(name?: string, type?: LoggerTypeData) {
        this.defaultName = Logger._getDefault();
        this.name = typeof name === 'string' && name.length ? name : '';
        const loggerType = redux.get<LoggerTypeData>(this.defaultName);
        this.type = type ? type : loggerType ?? Logger.type.debug;
    }

    static type: LoggerType = {
        debug: "debug",
        release: "release",
    }

    static init(type: LoggerTypeData) {
        redux.create(this._getDefault(), type ?? Logger.type.debug);
    }

    private static _getDefault(): string {
        return 'logger';
    }

    err(...obj: any): void {
        console.error(this.name ?? '', ...obj);
    }

    log(...obj: any): void {
        if (this.type === Logger.type.release) return;
        console.log(this.name ?? '', ...obj);
    }

    warn(...obj: any): void {
        if (this.type === Logger.type.release) return;
        console.warn(this.name ?? '', ...obj);
    }

    timeStart(name: string): void {
        if (this.type === Logger.type.release) return;
        console.time(name)
    }

    timeEnd(name: string): void {
        if (this.type === Logger.type.release) return;
        console.timeEnd(name)
    }

    timeLog(name: string, obj: any): void {
        if (this.type === Logger.type.release) return;
        console.timeLog(name, ...obj)
    }
}


export {
    Logger
}

export type {
    ILoggerClass,
    ILogger,
    LoggerType,
    LoggerTypeData,
}

