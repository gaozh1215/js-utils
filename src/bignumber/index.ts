import D from 'bignumber.js'


export interface IBignumber {
    readonly div: (num1: NumTypeData, num2: NumTypeData) => number
    readonly dividedBy: (num1: NumTypeData, num2: NumTypeData) => number
    readonly minus: (num1: NumTypeData, num2: NumTypeData) => number
    readonly multipliedBy: (num1: NumTypeData, num2: NumTypeData) => number
    readonly plus: (num1: NumTypeData, num2: NumTypeData) => number
    readonly times: (num1: NumTypeData, num2: NumTypeData) => number
}

export type NumTypeData = number | string

const bignumber: IBignumber = {
    /**
     * 除以
     * @param num1
     * @param num2
     */
    div(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).div(num2).toNumber()
    },
    /**
     * 除以
     * @param num1
     * @param num2
     */
    dividedBy(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).dividedBy(num2).toNumber()
    },

    /**
     * 乘以
     * @param num1
     * @param num2
     */
    multipliedBy(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).multipliedBy(num2).toNumber()
    },
    /**
     * 乘以
     * @param num1
     * @param num2
     */
    times(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).times(num2).toNumber()
    },
    /**
     * 加
     * @param num1
     * @param num2
     */
    plus(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).plus(num2).toNumber()
    },
    /**
     * 减
     * @param num1
     * @param num2
     */
    minus(num1: NumTypeData, num2: NumTypeData): number {
        return new D(num1).minus(num2).toNumber()
    },
}

export default bignumber
