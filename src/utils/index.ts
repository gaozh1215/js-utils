import {
    DateCountDownParams,
    FormatDataParams,
    FormatDataResult,
    FormatDataToJsonParams,
    IsAmountParams,
    IUtils,
    ToAmountParams, ToStrCutParams
} from "./props";
import bignumber from "../bignumber";
import {AES, enc, MD5, mode, pad} from "crypto-js";


class Utils implements IUtils {
    /**
     * 对比数组，返回 新旧差异
     * @param oldArr    {<T>[]} 旧数组
     * @param newArr    {<T>[]} 新数组
     * @param contrastFields    {string[]} 对比参数
     */
    contrastArrayToDiff<T>(oldArr: T[], newArr: T[], contrastFields?: string[]): {
        oldArrDiff: T[];
        newArrDiff: T[]
    } {
        const newList: T[] = [], oldList: T[] = [];
        if (!this.isArray(oldArr)) oldArr = [];
        if (!this.isArray(newArr)) newArr = [];
        if (!contrastFields) contrastFields = [];
        const isContrastAll = !contrastFields.length
        oldArr.map((item: any) => {
            if (isContrastAll) {
                if (newArr.findIndex(m => this.contrastJson(m, item)) < 0) oldList.push(item);
            } else {
                if (newArr.findIndex((m: any) => {
                    let hasExist = true;//  是否存在
                    if (contrastFields) contrastFields.map((name: string) => {
                        if (m[name] !== item[name]) hasExist = false;
                    })
                    return hasExist
                }) < 0) oldList.push(item);
            }
        })
        newArr.map((item: any) => {
            if (isContrastAll) {
                if (oldArr.findIndex(m => this.contrastJson(m, item)) < 0) newList.push(item);
            } else {
                if (oldArr.findIndex((m: any) => {
                    let hasExist = true;//  是否存在
                    if (contrastFields) contrastFields.map((name: string) => {
                        if (m[name] !== item[name]) hasExist = false;
                    })
                    return hasExist
                }) < 0) newList.push(item);
            }
        })

        return {newArrDiff: newList, oldArrDiff: oldList}
    }

    /**
     * 对比json 是否一致
     * @param jsonOne
     * @param jsonTwo
     */
    contrastJson<T>(jsonOne: T, jsonTwo: T): boolean {
        if (!this.isJson(jsonOne) || !this.isJson(jsonTwo)) return false;
        return JSON.stringify(jsonOne) === JSON.stringify(jsonTwo);
    }

    /**
     * 对比json 返回不同
     * @param oldJson
     * @param newJson
     */
    contrastJsonToDiff<T>(oldJson: T, newJson: T): T {
        if (this.contrastJson(oldJson, newJson)) return {} as T;
        const result: any = {};
        for (let item in newJson) {
            if (newJson[item] !== oldJson[item]) {
                if (typeof newJson[item] === 'number') result[item] = Number(newJson[item]);
                else result[item] = newJson[item]
            }
        }
        return result
    }

    /**
     * 对比版本
     * @param serverVersion
     * @param localVersion
     * @param serverBuild
     * @param localBuild
     */
    contrastVersion(serverVersion: string, localVersion: string, serverBuild?: string | number, localBuild?: string | number): boolean {
        const sList = serverVersion.split('.');
        const lList = localVersion.split('.');
        let sNum = 0, lNum = 0;
        for (let i = 0, len = sList.length; i < len; i++) {
            sNum = bignumber.plus(sNum, sList[i]);
            lNum = bignumber.plus(lNum, lList[i]);
            if (sNum > lNum) return true;
            else if (sNum < lNum) return false;
        }
        return Number(serverBuild) > Number(localBuild);
    }

    /**
     * 倒计时
     * @param endDate
     * @param params
     */
    dateCountDown(endDate: string | number, params?: DateCountDownParams): void {
        params = params || {};
        let format = params.format ?? 'dd hh:mm:ss';
        format = this._replaceDate(format);
        endDate = this._checkDate(endDate);
        if (!endDate) return;
        const dateJson = this.formatDateToJson(endDate);
        let prev = '';//上一条数据
        const end = dateJson.time ?? 0;
        if (typeof params.startCallback === "function") params.startCallback();
        const intTime = setInterval(() => {
            const curr = new Date().getTime();
            const time = end - curr;
            let dd = Math.floor(time / (1000 * 60 * 60 * 24));
            if (dd <= 0) dd = 0;
            let hh = Math.floor(time / (1000 * 60 * 60) % 24);
            if (hh <= 0) hh = 0;
            let mm = Math.floor(time / (1000 * 60) % 60);
            if (mm <= 0) mm = 0;
            let ss = Math.floor(time / 1000 % 60);
            if (ss <= 0) ss = 0;
            const resultTimeString = this.replaceStrByJson(format, {dd, HH: hh, hh, mm, ss});

            //  输出
            if (prev !== resultTimeString) {
                prev = resultTimeString;
                if (params && typeof params.callback === "function") params.callback(resultTimeString);
            }

            //  结束
            if (dd === 0 && hh === 0 && mm === 0 && ss === 0) {
                clearInterval(intTime);
                if (params && typeof params.endCallback === "function") params.endCallback();
            }
        }, 100);
    }

    // 检查时间
    private _checkDate(date: string | number): number {
        date = typeof date === 'number' ? date.toString() : date;
        if (!date.length) return 0;
        if (date.indexOf('0001/01/01') === 0) return 0;
        if (date.indexOf('-') >= 0) date = date.replace(/-/g, '/');
        return this.toNumber(date);
    }

    // 替换时间参数
    private _replaceDate(str: string): string {
        if (str.indexOf('yyyy') >= 0) str = str.replace('yyyy', '{yyyy}');
        else if (str.indexOf('yy') >= 0) str = str.replace('yy', '{yy}');
        if (str.indexOf('MM') >= 0) str = str.replace('MM', '{MM}');
        if (str.indexOf('dd') >= 0) str = str.replace('dd', '{dd}');
        if (str.indexOf('HH') >= 0) str = str.replace('HH', '{HH}');
        if (str.indexOf('hh') >= 0) str = str.replace('hh', '{hh}');
        if (str.indexOf('mm') >= 0) str = str.replace('mm', '{mm}');
        if (str.indexOf('ss') >= 0) str = str.replace('ss', '{ss}');
        return str
    }

    /**
     * 格式化时间
     * @param data
     * @param params
     */
    formatDate(data: string | number, params?: FormatDataParams): string {
        params = params || {};
        params.format = params.format || 'yyyy-MM-dd hh:mm:ss';
        const format = this._replaceDate(params.format);
        return this.replaceStrByJson(format, this.formatDateToJson(data, params));
    }

    /**
     * 格式化时间，使用格林威治时间
     * @param data
     * @param params
     */
    formatDateUTC(data: string | number, params?: FormatDataParams): string {
        params = params || {};
        params.format = params.format || 'yyyy-MM-dd hh:mm:ss';
        const value = this._checkDate(data);
        if (!value) return '';
        const timezoneOffset = new Date().getTimezoneOffset() * 60 * 1000;
        let time = new Date(value).getTime();
        time = new Date(time - timezoneOffset).getTime();
        const format = this._replaceDate(params.format);
        return this.replaceStrByJson(format, this.formatDateToJson(time, params));
    }

    /**
     * 格式化时间，使用格林威治时间
     * @param data
     * @param params
     */
    formatDateToUTC(data: string | number, params?: FormatDataParams): string {
        params = params || {};
        params.format = params.format || 'yyyy-MM-dd hh:mm:ss';
        const value = this._checkDate(data);
        if (!value) return '';
        const timezoneOffset = new Date().getTimezoneOffset() * 60 * 1000;
        let time = new Date(value).getTime();
        time = new Date(time + timezoneOffset).getTime();
        const format = this._replaceDate(params.format);
        return this.replaceStrByJson(format, this.formatDateToJson(time, params));
    }

    /**
     * 格式化时间 返回json数据
     * @param data
     * @param params
     */
    formatDateToJson(data: string | number, params?: FormatDataToJsonParams): FormatDataResult {
        params = params || {};
        const value = this._checkDate(data);
        const result: FormatDataResult = {
            yyyy: '', yy: '', MM: '', dd: '', HH: '', hh: '', mm: '', ss: '', day: 0, time: 0
        }
        if (!value) return result;
        const varDate = new Date(value);
        if (params.day) {
            if (typeof params.day && !isNaN(params.day)) varDate.setDate(varDate.getDate() + params.day);
        }
        result.yyyy = varDate.getFullYear().toString();
        result.yy = result.yyyy.substring(2, result.yyyy.length);
        result.MM = (varDate.getMonth() + 1).toString();
        result.dd = varDate.getDate().toString();
        result.HH = varDate.getHours().toString();
        result.hh = varDate.getHours().toString();
        result.mm = varDate.getMinutes().toString();
        result.ss = varDate.getSeconds().toString();
        result.day = varDate.getDay();
        result.time = varDate.getTime();
        result.MM = result.MM.length > 1 ? result.MM : `0${result.MM}`;
        result.dd = result.dd.length > 1 ? result.dd : `0${result.dd}`;
        result.HH = result.HH.length > 1 ? result.HH : `0${result.HH}`;
        result.hh = result.hh.length > 1 ? result.hh : `0${result.hh}`;
        result.mm = result.mm.length > 1 ? result.mm : `0${result.mm}`;
        result.ss = result.ss.length > 1 ? result.ss : `0${result.ss}`;
        return result;
    }

    /**
     * 是否是金额
     * @param data
     * @param params
     */
    isAmount(data: any, params?: IsAmountParams): boolean {
        params = params || {};
        //  是否是整数；默认 false
        if (typeof params.isInteger !== 'boolean') params.isInteger = false;
        //  是否可以为0；默认 false
        if (typeof params.canZero !== 'boolean') params.canZero = false;
        //  是否可以为负数；默认 false
        if (typeof params.isNegativeNum !== 'boolean') params.isNegativeNum = false;
        if (!this.isNumber(data)) return false;
        const value = Number(data);
        return !((!params.isNegativeNum && value < 0)
            || (!params.canZero && value === 0)
            || (params.isInteger && !this.isInteger(value)))
    }

    /**
     * 是否是数组
     * @param data
     */
    isArray(data: any): boolean {
        if (typeof data === 'object') {
            if (data.length) return true;
            else if (data.toString() === '') return true;
        }
        return false;
    }

    /**
     * 是否是整数
     * @param data
     */
    isInteger(data: any): boolean {
        if (!this.isNumber(data)) return false;
        return Math.round(data).toString() === data.toString();
    }

    /**
     * 是否是json
     * @param data
     */
    isJson(data: any): boolean {
        try {
            if (typeof data === 'string') {
                return !!JSON.parse(data);
            } else if (typeof data === 'object') {
                if (typeof data.length === "number") return false;
            }
            const jsonToString = JSON.stringify(data);
            const stringToJson = JSON.parse(jsonToString);
            return !!stringToJson;
        } catch (err) {
            return false
        }
    }

    /**
     * 是否是数字
     * @param data
     */
    isNumber(data: any): boolean {
        try {
            if (data === '') return false;
            const value = Number(data);
            return !isNaN(value);
        } catch (err) {
            return false
        }
    }

    /**
     * 判断string类型 value 值在第一位
     * @param data
     * @param value
     */
    isStrFirst(data: string, value: string | string[]): boolean {
        if (typeof value === "string") return data.indexOf(value) === 0;
        let result = false;
        if (value && value.length) value.map((item) => {
            if (data.indexOf(item) === 0) result = true;
        })
        return result;
    }

    /**
     * 判断string类型 value 值在第最后一位
     * @param data
     * @param value
     */
    isStrLast(data: string, value: string | string[]): boolean {
        if (typeof value === "string") return data.lastIndexOf(value) === (data.length - value.length);
        let result = false;
        if (value && value.length) value.map((item) => {
            if (data.lastIndexOf(item) === (data.length - item.length)) result = true;
        })
        return result;
    }

    /**
     * 替换或拼接Json到Url
     * @param url
     * @param json
     */
    replaceOrSpliceToUrlByJson(url: string, json: any): string {
        if (!this.isJson(json)) return '';
        let value = `${url}${url.indexOf('?') > 0 ? this.isStrLast(url, ['&']) ? '' : '&' : '?'}`;
        for (const item in json) {
            if (!json.hasOwnProperty(item)) continue;
            const name = `{${item}}`;
            if (value.indexOf(name) >= 0) {
                do {
                    value = value.replace(name, json[item]);
                } while (value.indexOf(name) >= 0)
            } else {
                value = `${value}${item}=${json[item]}&`
            }
        }
        if (this.isStrLast(value, ['?', '&'])) value = value.substring(0, value.length - 1);
        return value;
    }

    /**
     * 用json 替换 string
     * @param data
     * @param json
     */
    replaceStrByJson(data: string, json: any): string {
        if (!this.isJson(json)) return data;
        let value = data;
        for (const item in json) {
            if (!json.hasOwnProperty(item)) continue;
            const name = `{${item}}`;
            if (value.indexOf(name) >= 0) {
                do {
                    value = value.replace(name, json[item]);
                } while (value.indexOf(name) >= 0)
            }
        }
        return value;
    }

    /**
     * 转换金额
     * @param data
     * @param params
     */
    toAmount(data: string | number, params?: ToAmountParams): string {
        if (!this.isAmount(data, {canZero: true, isNegativeNum: true})) return '0'
        const value = this.toScientificCount(data);
        params = params || {};
        if (typeof params.decimalFold !== 'boolean') params.decimalFold = true;
        params.isConvert = typeof params.isConvert === 'boolean' ? params.isConvert : true;
        const min = typeof params.min === 'number' && this.isInteger(params.min) ? Number(params.min) : 2;
        params.max = typeof params.max === 'number' && this.isInteger(params.max) ? Number(params.max) : min > 4 ? Number(params.min) : 4;
        params.min = typeof params.min === 'number' && this.isInteger(params.min) ? Number(params.min) : Number(params.max) > 2 ? 2 : Number(params.max);
        const valueSplit = value.split('.');
        if (valueSplit[0].length > 16) {
            if (valueSplit.length > 1) {
                const valueDot = `0.${valueSplit[1]}`;
                const dot = this.toAmount(valueDot, params);
                return `${valueSplit[0]}${dot.substring(1, dot.length)}`;
            } else return valueSplit[0]
        }
        if (this.isInteger(value)) {
            // min 最小显示
            const valueStrMin = this.toNumber(value).toFixed(params.min);
            // 整数
            if (params.isConvert) return this.toAmountConvert(valueStrMin);
            return valueStrMin;
        } else {
            // 非整数
            const valueNumber = this.toNumber(value);
            const valueStr = this.toScientificCount(valueNumber);
            const dotIndex = valueStr.indexOf('.');
            if (params.decimalFold) {
                let valueNum = valueSplit[0];
                const valueDecimal = valueSplit[1];
                let valueCompared = '0';
                for (let i = 0, len = valueDecimal.length; i < len; i++) {
                    if (valueDecimal.indexOf(valueCompared) === 0) valueCompared = `${valueCompared}0`;
                    else break;
                }
                const lengthCompared = valueCompared.length - 1;
                const lengthDecimal = valueDecimal.length;
                if (lengthCompared > 3) {
                    if (params.isConvert) valueNum = this.toAmountConvert(valueNum);
                    const lastLength = (lengthDecimal - lengthCompared) > 3 ? (lengthCompared + 3) : lengthDecimal;
                    return `${valueNum}.0{${lengthCompared}}${valueDecimal.substring(lengthCompared, lastLength)}`;
                }
            }
            if ((dotIndex < 0) || (valueStr.length - (dotIndex < 0 ? 0 : dotIndex) <= params.min ?? 0)) {
                // min 最小显示
                const valueMin = valueNumber.toFixed(params.min);
                if (params.isConvert) return this.toAmountConvert(valueMin);
                return valueMin;
            } else {
                // max 最大显示
                const valueSplit = valueStr.split('.');
                let dotStr = `.${valueSplit[1].substring(0, params.max)}`;
                if (params.max === 0) dotStr = '';
                const valueStrMax = `${valueSplit[0]}${dotStr}`;
                if (params.isConvert) return this.toAmountConvert(valueStrMax);
                return valueStrMax;
            }
        }
    }

    /**
     * 金额转换 1000 => 1,000
     * @param data
     */
    toAmountConvert(data: string | number): string {
        if (!this.isAmount(data, {canZero: true, isNegativeNum: true})) return '';
        const valueStr = data.toString();
        //金额转换 分->元 保留2位小数 并每隔3位用逗号分开 1,234.56
        let d = valueStr.indexOf(".");
        if (d < 0) d = valueStr.length;
        let intSum = valueStr.substring(0, d).replace(/\B(?=(?:\d{3})+$)/g, ',');//取到整数部分
        let dot = '';
        if (valueStr.indexOf(".") >= 0) dot = valueStr.substring(valueStr.length, valueStr.indexOf("."));//取到小数部分搜索
        return (intSum + dot).toString();
    }

    /**
     * 深度复制 json
     * @param data
     */
    toCopyDeepJson<T>(data: T): T {
        if (!this.isJson(data)) return {} as T;
        return JSON.parse(JSON.stringify(data));
    }


    /**
     * 深度复制 array
     * @param data
     */
    toCopyDeepArray<T>(data: T[]): T[] {
        if (!this.isArray(data)) return [];
        const list: T[] = [];
        data.map(item => list.push(item));
        return list;
    }

    /**
     * 对称解密
     * @param data
     * @param key
     * @param iv
     */
    toDecrypt(data: string, key: string, iv?: string): string {
        if (key.length < 8) return '';
        try {
            const keyHex = enc.Hex.parse(key);
            const valHex = enc.Hex.parse(data);
            const valBase64 = enc.Base64.stringify(valHex);
            const bytes = AES.decrypt(valBase64, keyHex, {
                iv: enc.Hex.parse(iv ?? this.toMD5('js-utils')),
                mode: mode.CBC,
                padding: pad.Pkcs7
            });
            const originalText = bytes.toString(enc.Utf8);
            if (!originalText) return '';
            return originalText;
        } catch (err) {
            return '';
        }
    }

    /**
     * 对称加密
     * @param data
     * @param key
     * @param iv
     */
    toEncrypt(data: string, key: string, iv?: string): string {
        if (typeof data !== 'string' || data.length === 0) return data;
        if (key.length < 8) return '';
        const keyHex = enc.Hex.parse(key);
        const encrypt = AES.encrypt(data, keyHex, {
            iv: enc.Hex.parse(iv ?? this.toMD5('js-utils')),
            mode: mode.CBC,
            padding: pad.Pkcs7
        });
        return encrypt.ciphertext.toString();
    }

    /**
     * 将search转换成json
     * @param search
     */
    toJsonBySearch<T>(search: string): T {
        const result: any = {} as T;
        if (!search.length) return result;
        const strA = search.indexOf('?') >= 0 ? search.split('?')[1] : search;
        const strB = strA.split('&');
        for (let i = 0, len = strB.length; i < len; i++) {
            const item = strB[i].split('=');
            result[item[0]] = typeof item[1] === 'undefined' ? '' : item[1];
        }
        return result;
    }

    /**
     * md5 加密
     * @param data
     */
    toMD5(data: string): string {
        return MD5(data).toString()
    }

    /**
     * 获取number
     * @param data
     */
    toNumber(data: any): number {
        if (!this.isNumber(data)) return 0;
        const valueStr = this.toScientificCount(data);
        return Number(valueStr);
    }

    /**
     * 获取随机数
     * @param count
     */
    toRandom(count?: number): string {
        count = count ?? 4;
        const data = Math.floor(Math.random() * Math.pow(10, count)).toString();
        if (data.length !== count) return this.toRandom(count);
        return data;
    }

    /**
     * 科学计数转换
     * @param data
     */
    toScientificCount(data: string | number): string {
        data = Number(data);
        let m = data.toExponential().match(/\d(?:\.(\d*))?e([+-]\d+)/) as any;
        return data.toFixed(Math.max(0, (m[1] || '').length - m[2]));
    }

    /**
     * 将json转换成search
     * @param data
     */
    toSearchByJson(data: any): string {
        if (!this.isJson(data)) return '';
        let value: string = '';
        for (const item in data) {
            if (!data.hasOwnProperty(item)) continue;
            value += `${item}=${data[item]}&`;
        }
        if (value) value = value.substring(0, value.length - 1);
        return value;
    }

    /**
     * 随机洗牌
     * @param arr
     */
    toShuffle<T>(arr: T[]): T[] {
        let i = arr.length;
        while (i) {
            let j = Math.floor(Math.random() * i--);
            [arr[j], arr[i]] = [arr[i], arr[j]];
        }
        return arr;
    }

    /**
     * 裁剪
     * @param data
     * @param params
     */
    toStrCut(data: string, params?: ToStrCutParams): string {
        params = params || {};
        const count: number = params.count || 10;
        const unit: string = params.unit || '...';
        const countLeft: number = typeof params.countLeft === "number" && this.isInteger(params.countLeft) ? Number(params.countLeft) : count;
        const countRight: number = typeof params.countRight === "number" && this.isInteger(params.countRight) ? Number(params.countRight) : count;
        if (data.length <= (countLeft + countRight)) return data;
        return `${data.substring(0, countLeft)}${unit}${data.substring(data.length - countRight, data.length)}`
    }

    toStrFirst(data: string, length: number | undefined): string {
        length = length ?? 1;
        if (!data.length || data.length < length) return '';
        return data.substring(0, length);
    }

    toStrLast(data: string, length: number | undefined): string {
        length = length ?? 1;
        if (!data.length || data.length < length) return '';
        return data.substring(data.length - length, data.length);
    }

}

const utils: IUtils = new Utils();
export default utils
