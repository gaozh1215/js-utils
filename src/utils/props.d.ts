export interface IUtils extends UtilsIs, UtilsTo, UtilsReplace, UtilsFormat, UtilsContrast {
    //  倒计时
    readonly dateCountDown: (endDate: string | number, params?: DateCountDownParams) => void
}

export type DateCountDownParams = {
    //  格式化样式 （yyyy-MM-dd hh:mm:ss）
    format?: string
    startCallback?: () => void
    endCallback?: () => void
    callback?: (date: string) => void
}

interface UtilsIs {
    readonly isArray: (data: any) => boolean
    readonly isJson: (data: any) => boolean
    readonly isNumber: (data: any) => boolean
    readonly isInteger: (data: any) => boolean
    readonly isAmount: (data: any, params?: IsAmountParams) => boolean
    readonly isStrFirst: (data: string, value: string | string[]) => boolean
    readonly isStrLast: (data: string, value: string | string[]) => boolean
}

interface UtilsTo {
    //  md5 加密
    readonly toMD5: (data: string) => string
    //  对称加密
    readonly toEncrypt: (data: string, key: string, iv?: string) => string
    //  对称解密
    readonly toDecrypt: (data: string, key: string, iv?: string) => string

    readonly toStrFirst: (data: string, length?: number) => string
    readonly toStrLast: (data: string, length?: number) => string
    //  裁剪
    readonly toStrCut: (data: string, params?: ToStrCutParams) => string
    //  获取number
    readonly toNumber: (data: any) => number
    //  转换金额
    readonly toAmount: (data: string | number, params?: ToAmountParams) => string
    //  金额转换 1000 => 1,000
    readonly toAmountConvert: (data: string | number) => string
    //  科学计数转换
    readonly toScientificCount: (data: string | number) => string
    //  获取随机数
    readonly toRandom: (count?: number) => string
    //  随机洗牌
    readonly toShuffle: <T>(arr: T[]) => T[]
    //  深度复制 json
    readonly toCopyDeepJson: <T>(data: T) => T
    //  深度复制 array
    readonly toCopyDeepArray: <T>(data: T[]) => T[]
    //  将json转换成search
    readonly toSearchByJson: (data: any) => string
    //  将search转换成json
    readonly toJsonBySearch: <T>(search: string) => T
}

interface UtilsReplace {
    //  用 json 替换 string
    readonly replaceStrByJson: (data: string, json: any) => string
    //  替换或拼接 json 到 url
    readonly replaceOrSpliceToUrlByJson: (url: string, json: any) => string
}

interface UtilsFormat {
    //  格式化时间
    readonly formatDate: (data: string | number, params?: FormatDataParams) => string
    //  格式化时间，时区偏移量，格林威治时间
    readonly formatDateUTC: (data: string | number, params?: FormatDataParams) => string
    //  格式化时间，时区偏移量，格林威治时间
    readonly formatDateToUTC: (data: string | number, params?: FormatDataParams) => string
    //  获取时间，返回json格式
    readonly formatDateToJson: (data: string | number, params?: FormatDataToJsonParams) => FormatDataResult
}

interface UtilsContrast {
    //  对比json 是否一致
    readonly contrastJson: <T>(jsonOne: T, jsonTwo: T) => boolean
    //  对比json 返回不同
    readonly contrastJsonToDiff: <T>(oldJson: T, newJson: T) => T
    //  对比数组，返回不同
    readonly contrastArrayToDiff: <T>(oldArr: T[], newArr: T[], contrastFields?: string[]) => {
        oldArrDiff: T[], newArrDiff: T[]
    }
    //  版本对比
    readonly contrastVersion: (serverVersion: string, localVersion: string, serverBuild?: string | number, localBuild?: string | number) => boolean
}

/**
 * isAmount params
 */
export type IsAmountParams = {
    //  是整数
    isInteger?: boolean
    //  可以包括 0
    canZero?: boolean
    //  是负数
    isNegativeNum?: boolean
}

/**
 * toStrCut params
 */
export type ToStrCutParams = {
    //  左右个数
    count?: number
    //  左侧个数
    countLeft?: number
    //  右侧个数
    countRight?: number
    //  替换字符
    unit?: string
}

/**
 * toAmount params
 */
export type ToAmountParams = {
    //  是否金额转换 1000 => 1,000
    isConvert?: boolean
    //  小数位，最小显示
    min?: number
    //  小数位，最大显示
    max?: number
    //  小数位折叠
    decimalFold?: boolean
}

export type FormatDataParams = {
    //  加减天数
    day?: number
    //  格式化样式 （yyyy-MM-dd hh:mm:ss）
    format?: string
}

export type FormatDataToJsonParams = {
    //  加减天数
    day?: number
}

export type FormatDataResult = {
    //  年 2020
    yyyy: string
    //  年 20
    yy: string
    // 月
    MM: string
    //  日
    dd: string
    //  小时（24小时）
    HH: string
    //  小时（12小时）
    hh: string
    //  分钟
    mm: string
    //  秒
    ss: string
    //  星期几
    day: number
    //  时间戳
    time: number
}
