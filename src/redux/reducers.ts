import {legacy_createStore as createStore} from 'redux'
import config, {ReduxActionData} from './config';


const reducers = (state: any = {}, action: ReduxActionData) => {
    let res = state[action.name];
    let data: any = Object.assign({}, state);
    if (!action.name) return data;
    //  初始化名称
    const _initDefaultName = `${action.name}_**_init`;

    if (action.type === config.type.create) {  //  判断类型为：创建
        if (typeof res !== "undefined") {
            console.warn('redux', `type:create`, `name：${action.name}`, `已经被创建，存在数据; typeof res：${typeof res}`);
            return data;
        }
    } else if (action.type === config.type.update) { //  判断类型为：修改
        if (typeof res === 'undefined') {   //  数据为undefined，不可修改
            console.warn('redux', `type:update`, `name：${action.name}`, `存在空数据; typeof res：${typeof res}`);
            return data;
        }
    } else if (action.type === config.type.removeAndInit) { //  判断类型为：删除重新初始化
        const initData = data[_initDefaultName];
        if (!initData) {   //  未找到初始化参数
            console.warn('redux', `type:removeAndInit`, `name：${action.name}`, `未找到初始化参数; typeof res：${typeof initData}`);
            return data;
        }
    }


    //  执行操作
    if (action.type === config.type.remove) {  //  删除
        delete data[action.name];
        if (data[_initDefaultName]) delete data[_initDefaultName];

    } else if (action.type === config.type.removeAndInit) {    //  删除并重新初始化
        data[action.name] = data[_initDefaultName];

    } else {    //  修改
        data[action.name] = action.params;

        if (action.type === config.type.create && action.isInit)    //  创建的时候添加默认初始化数据
            data[_initDefaultName] = action.params;
    }

    return data
}

export default createStore(reducers);
// export default configureStore({
//     reducer: reducers,
// })
