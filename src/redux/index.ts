import config from './config';
import reducers from './reducers';
import {IRedux} from "./props";


const redux: IRedux = {
    /**
     * 监听
     * @param name  {string}    名称
     * @param callback  {function}  回调方法
     */
    listener: <T>(name: any, callback: (params: T) => void) => {
        let lastData: any = undefined;
        reducers.subscribe(() => {
            if (typeof callback === 'function') {
                const result = redux.get(name) as T;
                if (lastData === result) return;
                lastData = result
                callback(result);
            }
        })
    },
    /**
     * 获取全部数据
     */
    all: (): any => {
        return reducers.getState()
    },
    /**
     * 根据名称获取数据
     * @param name  {string}    名称
     */
    get: <T>(name: string): T => {
        return redux.all()[name] as T
    },
    /**
     * 创建
     * @param name  {string}    名称
     * @param params
     * @param isInit    {boolean}   是否存储初始化值
     */
    create: <T>(name: string, params: T, isInit?: boolean) => {
        reducers.dispatch(config.action.create(name, params, isInit));
        return redux.get<T>(name);
    },
    /**
     * 修改
     * @param name  {string}    名称
     * @param params
     */
    update: <T>(name: string, params: T) => {
        reducers.dispatch(config.action.update(name, params));
        return redux.get<T>(name);
    },
    /**
     * 删除
     * @param name  {string}    名称
     */
    remove: (name: string): void => {
        reducers.dispatch(config.action.remove(name))
    },
    /**
     * 删除并重新初始化，必须在create()时，isInit = true 才可使用方法，否则报错
     * @param name  {string}    名称
     */
    removeAndInit: (name: string): void => {
        reducers.dispatch(config.action.removeAndInit(name))
    },
}

export default redux

