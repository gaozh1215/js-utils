export interface IRedux {
    /**
     * 监听
     * @param name  {string}    名称
     * @param callback  {function}  回调方法
     */
    readonly listener: <T>(name: string, callback: (params: T) => void) => void
    /**
     * 获取全部数据
     */
    readonly all: () => any
    /**
     * 根据名称获取数据
     * @param name  {string}    名称
     */
    readonly get: <T>(name: string) => T
    /**
     * 创建
     * @param name  {string}    名称
     * @param params
     * @param isInit    {boolean}   是否保存初始化值
     */
    readonly create: <T>(name: string, params: T, isInit?: boolean) => T
    /**
     * 修改
     * @param name  {string}    名称
     * @param params
     */
    readonly update: <T>(name: string, params: T) => T
    /**
     * 删除
     * @param name  {string}    名称
     */
    readonly remove: (name: string) => void
    /**
     * 删除并重新初始化，必须在create()时，isInit = true 才可使用方法，否则报错
     * @param name  {string}    名称
     */
    readonly removeAndInit: (name: string) => void
}
