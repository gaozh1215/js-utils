export type ReduxType = {
    //  创建
    readonly create: 'create_data'
    //  修改
    readonly update: 'update_data'
    //  删除
    readonly remove: 'remove_data'
    //  删除并且重新初始化
    readonly removeAndInit: 'removeAndInit_data'
}
export type ReduxTypeData = 'create_data' | 'update_data' | 'remove_data' | 'removeAndInit_data'

export type ReduxAction = {
    create: (name: string, params: any, isInit?: boolean) => ReduxActionData
    update: (name: string, params: any) => ReduxActionData
    remove: (name: string) => ReduxActionData
    removeAndInit: (name: string) => ReduxActionData
}
export type ReduxActionData = {
    //  类型
    type: ReduxTypeData
    //  名称
    name: string
    //  参数
    params?: any
    //  是否保留初始化值
    isInit?: boolean
}

const type: ReduxType = {
    create: "create_data",
    update: 'update_data',
    remove: 'remove_data',
    removeAndInit: "removeAndInit_data",
}

const action: ReduxAction = {
    create: (name: string, params: any, isInit?: boolean): ReduxActionData => {
        return {type: type.create, name, params, isInit: typeof isInit === 'boolean' ? isInit : false}
    },
    update: (name: string, params: any): ReduxActionData => {
        return {type: type.update, name, params}
    },
    remove: (name: string): ReduxActionData => {
        return {type: type.remove, name}
    },
    removeAndInit: (name: string): ReduxActionData => {
        return {type: type.removeAndInit, name}
    },
}

export default {
    type,
    action
}
