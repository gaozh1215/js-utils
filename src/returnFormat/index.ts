export interface IReturnFormat<T> {
    readonly hasSucceed: () => boolean
    readonly setResult: (data: T, msg?: string) => void
    readonly setMessage: (msg: string) => void
    readonly setError: (msg: string) => void
    readonly end: () => ReturnFormatData<T>
}

export type ReturnFormatData<T> = {
    code: number
    rs: T | undefined
    msg: string
}

class ReturnFormat<T> implements IReturnFormat<T> {
    private readonly result: ReturnFormatData<T>

    constructor() {
        this.result = {code: 0, msg: '', rs: undefined};
    }

    end(): ReturnFormatData<T> {
        return this.result;
    }

    hasSucceed(): boolean {
        return this.result.code === 0;
    }

    setError(msg: string): void {
        this.result.code = -1;
        this.result.rs = undefined
        this.setMessage(msg)
    }

    setMessage(msg: string): void {
        this.result.msg = msg ?? ''
    }

    setResult(data: T, msg?: string): void {
        this.result.rs = data;
        this.setMessage(msg ?? '')
    }
}

export default ReturnFormat

