import {utils} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';

describe('tools', () => {
    it('contrastJson', function () {
        const jsonOne = {a: 1, b: '2'};
        const jsonTwo = {a: 2, b: '1'};
        const data1 = utils.contrastJson(jsonOne, jsonTwo);
        strictEqual(data1, false);
        const data2 = utils.contrastJson(jsonOne, jsonOne);
        strictEqual(data2, true);
    });
    it('contrastJsonToDiff', function () {
        const oldJson = {
            "_id": "id-browser-etherscan",
            "desc": null,
            "hasDisplay": true,
            "link": "https://cn.etherscan.com",
            "logo": "https://res.kahf.app/configFiles/images/browser/eth.png",
            "name": "Etherscan",
            "b": 0
        }
        const newJson = {
            "_id": "id-browser-etherscan",
            "link": "https://cn.etherscan.com",
            "logo": "https://res.kahf.app/configFiles/images/browser/eth.png",
            "name": "Etherscan",
            "b": 1
        }
        const data = utils.contrastJsonToDiff(oldJson, newJson);
        console.log('contrastJsonToDiff', data);
        strictEqual(JSON.stringify(data), JSON.stringify({b: 1}));
    });
    it('contrastJsonToDiff', function () {
        const oldArr = [
            {a: 1, b: 1, c: 1},
            {a: 2, b: 2, c: 2},
            {a: 3, b: 3, c: 3},
            {a: 4, b: 4, c: 4},
            {a: 5, b: 5, c: 5},
            {a: 6, b: 6, c: 6}
        ];
        const newArr = [
            {a: 1, b: 1, c: 1},
            {a: 2, b: 2, c: 2},
            {a: 3, b: 3, c: 3},
            {a: 4, b: 4, c: 4},
            {a: 5, b: 5, c: 5},
        ]
        const data = utils.contrastArrayToDiff(oldArr, newArr);
        console.log(data);
        // strictEqual(JSON.stringify(data), JSON.stringify({b: 1}));
    });
    it('contrastVersion', function () {
        const serverVersion = '1.2.0';
        const localVersion = '1.1.10';
        const serverBuild = 123;
        const localBuild = 122;
        const data1 = utils.contrastVersion(serverVersion, localVersion);
        strictEqual(data1, true);
        const data2 = utils.contrastVersion(serverVersion, '1.2.0');
        strictEqual(data2, false);
        const data3 = utils.contrastVersion(serverVersion, '1.2.1');
        strictEqual(data3, false);
        const data4 = utils.contrastVersion(serverVersion, localVersion, serverBuild, localBuild);
        strictEqual(data4, true);
        const data5 = utils.contrastVersion(serverVersion, localVersion, serverBuild, 123);
        strictEqual(data5, true);
        const data6 = utils.contrastVersion(serverVersion, localVersion, serverBuild, 124);
        strictEqual(data6, true);
        const data7 = utils.contrastVersion(serverVersion, '1.2.0', serverBuild, localBuild);
        strictEqual(data7, true);
        const data8 = utils.contrastVersion(serverVersion, '1.2.0', serverBuild, 123);
        strictEqual(data8, false);
        const data9 = utils.contrastVersion(serverVersion, '1.2.0', serverBuild, 124);
        strictEqual(data9, false);
        const data10 = utils.contrastVersion(serverVersion, '1.2.1', serverBuild, localBuild);
        strictEqual(data10, false);
        const data11 = utils.contrastVersion(serverVersion, '1.2.1', serverBuild, 123);
        strictEqual(data11, false);
        const data12 = utils.contrastVersion(serverVersion, '1.2.1', serverBuild, 124);
        strictEqual(data12, false);
    });
    it('formatDate', function () {
        const time = new Date(1676516107461);
        const data = utils.formatDate(time.getTime());
        strictEqual(data, '2023-02-16 10:55:07')
    });
    it('formatDateTimezoneOffset', function () {
        const time = new Date('2023-01-01 00:00:00').getTime();
        const data = utils.formatDateUTC(time);
        const dataToUTC = utils.formatDateToUTC(time);
        strictEqual(data, '2023-01-01 08:00:00')
        strictEqual(dataToUTC, '2022-12-31 16:00:00')
        // const currTimer = new Date().getTime();
        // const timezoneOffsetTimer = utils.formatDateUTC(currTimer)
        // console.log('当前北京时间：', utils.formatDate(currTimer), currTimer)
        // console.log('格林威治时间：', timezoneOffsetTimer, new Date(timezoneOffsetTimer).getTime())
    });
    it('formatDateToJson', function () {
        const time = new Date(1676516107461);
        const data = utils.formatDateToJson(time.getTime());
        const jsonData = {
            yyyy: '2023',
            yy: '23',
            MM: '02',
            dd: '16',
            HH: '10',
            hh: '10',
            mm: '55',
            ss: '07',
            day: 4,
            time: 1676516107461
        };
        strictEqual(utils.contrastJson(data, jsonData), true);
    });
    it('isAmount', function () {
        const amount1 = '1';
        const amount2 = '1.1';
        const amount3 = '1.1.1';
        const amount4 = '1.00';
        const amount5 = '0';
        const amount6 = '-1';
        const data1 = utils.isAmount(amount1);
        strictEqual(data1, true);
        const data2 = utils.isAmount(amount2);
        strictEqual(data2, true);
        const data3 = utils.isAmount(amount3);
        strictEqual(data3, false);
        const data4 = utils.isAmount(amount4);
        strictEqual(data4, true);
        const data44 = utils.isAmount(amount5);
        strictEqual(data44, false);
        const data444 = utils.isAmount(amount6);
        strictEqual(data444, false);

        const data5 = utils.isAmount(amount1, {isInteger: true});
        strictEqual(data5, true);
        const data6 = utils.isAmount(amount2, {isInteger: true});
        strictEqual(data6, false);
        const data7 = utils.isAmount(amount3, {isInteger: true});
        strictEqual(data7, false);
        const data8 = utils.isAmount(amount4, {isInteger: true});
        strictEqual(data8, true);
        const data88 = utils.isAmount(amount5, {isInteger: true});
        strictEqual(data88, false);
        const data888 = utils.isAmount(amount6, {isInteger: true});
        strictEqual(data888, false);

        const data9 = utils.isAmount(amount1, {canZero: true});
        strictEqual(data9, true);
        const data10 = utils.isAmount(amount2, {canZero: true});
        strictEqual(data10, true);
        const data11 = utils.isAmount(amount3, {canZero: true});
        strictEqual(data11, false);
        const data12 = utils.isAmount(amount4, {canZero: true});
        strictEqual(data12, true);
        const data13 = utils.isAmount(amount5, {canZero: true});
        strictEqual(data13, true);
        const data14 = utils.isAmount(amount6, {canZero: true});
        strictEqual(data14, false);

        const data15 = utils.isAmount(amount1, {isNegativeNum: true});
        strictEqual(data15, true);
        const data16 = utils.isAmount(amount2, {isNegativeNum: true});
        strictEqual(data16, true);
        const data17 = utils.isAmount(amount3, {isNegativeNum: true});
        strictEqual(data17, false);
        const data18 = utils.isAmount(amount4, {isNegativeNum: true});
        strictEqual(data18, true);
        const data19 = utils.isAmount(amount5, {isNegativeNum: true});
        strictEqual(data19, false);
        const data20 = utils.isAmount(amount6, {isNegativeNum: true});
        strictEqual(data20, true);
    });
    it('isArray', function () {
        const arr1 = [];
        const arr2 = [1];
        const arr3 = [{a: 1}];
        const arr4 = {};
        const arr5 = undefined;
        const data1 = utils.isArray(arr1);
        strictEqual(data1, true);
        const data2 = utils.isArray(arr2);
        strictEqual(data2, true);
        const data3 = utils.isArray(arr3);
        strictEqual(data3, true);
        const data4 = utils.isArray(arr4);
        strictEqual(data4, false);
        const data5 = utils.isArray(arr5);
        strictEqual(data5, false);
    });
    it('isInteger', function () {
        const int1 = 1;
        const int2 = '1';
        const int3 = '1.1';
        const int4 = 'a';
        const data1 = utils.isInteger(int1);
        strictEqual(data1, true)
        const data2 = utils.isInteger(int2);
        strictEqual(data2, true)
        const data3 = utils.isInteger(int3);
        strictEqual(data3, false)
        const data4 = utils.isInteger(int4);
        strictEqual(data4, false)
    });
    it('isJson', function () {
        const a = `{"yyyy":"2000","MM":"12","dd":"10"}`;
        const b = {yyyy: '2000', MM: '12', dd: '10'};
        const data1 = utils.isJson(a);
        strictEqual(data1, true);
        const data2 = utils.isJson(b);
        strictEqual(data2, true);
        const data3 = utils.isJson([1]);
        strictEqual(data3, false);
        const data4 = utils.isJson(undefined);
        strictEqual(data4, false);
        const data5 = utils.isJson(null);
        strictEqual(data5, false);
        const data6 = utils.isJson([]);
        strictEqual(data6, false);
        const data7 = utils.isJson({});
        strictEqual(data7, true);
    });
    it('isNumber', function () {
        const amount1 = '1';
        const amount2 = '1.1';
        const amount3 = '1.1.1';
        const amount4 = '1.00';
        const amount5 = 'a';
        const data1 = utils.isNumber(amount1);
        strictEqual(data1, true)
        const data2 = utils.isNumber(amount2);
        strictEqual(data2, true)
        const data3 = utils.isNumber(amount3);
        strictEqual(data3, false)
        const data4 = utils.isNumber(amount4);
        strictEqual(data4, true)
        const data5 = utils.isNumber(amount5);
        strictEqual(data5, false)
    });
    it('isStrFirst', function () {
        const str1 = '123456';
        const data1 = utils.isStrFirst(str1, ['1', '2']);
        strictEqual(data1, true);
        const data2 = utils.isStrFirst(str1, ['12']);
        strictEqual(data2, true);
        const data3 = utils.isStrFirst(str1, ['6']);
        strictEqual(data3, false);
        const data4 = utils.isStrFirst(str1, ['12345']);
        strictEqual(data4, true);
    });
    it('isStrLast', function () {
        const str1 = '123456';
        const data1 = utils.isStrLast(str1, ['1', '2']);
        strictEqual(data1, false);
        const data2 = utils.isStrLast(str1, ['12']);
        strictEqual(data2, false);
        const data3 = utils.isStrLast(str1, ['6']);
        strictEqual(data3, true);
        const data4 = utils.isStrLast(str1, ['456']);
        strictEqual(data4, true);
    });
    it('replaceOrSpliceToUrlByJson', function () {
        const url1 = 'abcdef';
        const url2 = 'abcdef?a={a}';
        const url3 = 'abcdef?a={a}&b={b}';
        const url = 'abcdef?a=1&b=2'
        const params = {a: 1, b: 2};
        const data1 = utils.replaceOrSpliceToUrlByJson(url1, params);
        strictEqual(data1, url)
        const data2 = utils.replaceOrSpliceToUrlByJson(url2, params);
        strictEqual(data2, url)
        const data3 = utils.replaceOrSpliceToUrlByJson(url3, params);
        strictEqual(data3, url)
    });
    it('replaceStrByJson', function () {
        const a = '{yyyy}-{MM}-{dd}-{dd}';
        const b = {yyyy: '2000', MM: '12', dd: '10'};
        const data = utils.replaceStrByJson(a, b);
        strictEqual(data, '2000-12-10-10');
    });
    it('toJsonBySearch', function () {
        const search = '?a=1&b=&c=3';
        const data = utils.toJsonBySearch(search);
        strictEqual(utils.contrastJson(data, {a: '1', b: '', c: '3'}), true);
    });
    it('toAmount', function () {
        const amount1 = '123456789';
        const amount2 = '123456789.1234567890';
        const amount3 = undefined;
        const data1 = utils.toAmount(amount1);
        strictEqual(data1, '123,456,789.00');
        const data2 = utils.toAmount(amount1, {min: 4});
        strictEqual(data2, '123,456,789.0000');
        const data22 = utils.toAmount(amount1, {min: 0});
        strictEqual(data22, '123,456,789');
        const data3 = utils.toAmount(amount1, {min: 4, isConvert: false});
        strictEqual(data3, '123456789.0000');
        const data4 = utils.toAmount(amount2);
        strictEqual(data4, '123,456,789.1234');
        const data5 = utils.toAmount(amount2, {max: 6});
        strictEqual(data5, '123,456,789.123456');
        const data6 = utils.toAmount(amount2, {max: 6, isConvert: false});
        strictEqual(data6, '123456789.123456');
        const data7 = utils.toAmount(amount3);
        strictEqual(data7, '0');
        const data8 = utils.toAmount('1200.0');
        console.log(data8)
        const amount = utils.toAmount(5e-20 * 100);
        console.log('amount', amount);
    });
    it('toAmountConvert', function () {
        const amount = '123456789';
        const data = utils.toAmountConvert(amount);
        strictEqual(data, '123,456,789');
    });
    it('toCopyDeepJson', function () {
        const a = {a: 1};
        const b = a;
        b.a = 2;
        strictEqual(a.a, 2);
        strictEqual(b.a, 2);
        const c = utils.toCopyDeepJson(a);
        c.a = 3;
        strictEqual(a.a, 2);
        strictEqual(c.a, 3);
    });
    it('toDecrypt', function () {
        const value = '2adbd630cb4653e3763c1b8477d11fba';
        const pwd = utils.toMD5('1');
        const a = utils.toDecrypt(value, pwd);
        console.log('toDecrypt', a)
    });
    it('toEncrypt', function () {
        const value = '1';
        const pwd = utils.toMD5('1');
        const data = utils.toEncrypt(value, pwd);
        console.log('toEncrypt', data);
    });
    it('toMd5', function () {

    });
    it('toNumber', function () {

    });
    it('toRandom', function () {

    });
    it('toScientificCount', function () {

    });
    it('toSearchByJson', function () {
        const a = {a: 1, b: 2, c: 3};
        const data = utils.toSearchByJson(a);
        strictEqual(data, 'a=1&b=2&c=3');
    });
    it('toShuffle', function () {

    });
    it('toStrCut', function () {
        const value1 = '1234567890';
        const value2 = 'abcdefghijklmnopqrstuvwxyz';
        const data1 = utils.toStrCut(value1);
        strictEqual(data1, '1234567890');
        const data2 = utils.toStrCut(value2);
        strictEqual(data2, 'abcdefghij...qrstuvwxyz');
        const data3 = utils.toStrCut(value1, {count: 3, unit: '-'});
        strictEqual(data3, '123-890');
        const data4 = utils.toStrCut(value2, {countLeft: 5, countRight: 3, unit: '+'});
        strictEqual(data4, 'abcde+xyz');
        const data5 = utils.toStrCut(value2, {countLeft: 5, countRight: 0, unit: '+'});
        strictEqual(data5, 'abcde+');
    });
    it('toStrFirst', function () {
        const value = '1234567890';
        const data = utils.toStrFirst(value);
        strictEqual(data, '1');
        const data1 = utils.toStrFirst(value, 2);
        strictEqual(data1, '12');
        const data2 = utils.toStrFirst(value, 11);
        strictEqual(data2, '');
    });
    it('toStrLast', function () {
        const value = '1234567890';
        const data = utils.toStrLast(value);
        strictEqual(data, '0');
        const data1 = utils.toStrLast(value, 2);
        strictEqual(data1, '90');
        const data2 = utils.toStrLast(value, 11);
        strictEqual(data2, '');
    });
    it('dateCountDown', function () {
        const time = new Date();
        time.setSeconds(time.getSeconds() + 3);
        utils.dateCountDown(time.getTime(), {
            startCallback: () => console.log('开始'),
            callback: (val: string) => console.log(val),
            endCallback: () => console.log('结束'),
            format: 'ss',
        })
    });
})
