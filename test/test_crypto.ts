import {crypto} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';

describe('test_crypto', () => {
    it('mdt5', function () {
        const data = crypto.mdt5('1234567890');
        strictEqual(data, 'e807f1fcf82d132f9bb018ca6738a19f');
    });
    it('base64Encrypt', function () {
        const data = crypto.base64Encrypt('1234567890');
        strictEqual(data, 'MTIzNDU2Nzg5MA==');
    });
    it('base64Decrypt', function () {
        const data = crypto.base64Decrypt('MTIzNDU2Nzg5MA==');
        strictEqual(data, '1234567890');
    });
})
