import {ReturnFormat} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';

type FormatParams = {
    a: string
    b: number
}
describe('ReturnFormat', () => {
    it('setResult', function () {
        const returnFormat = new ReturnFormat<FormatParams>();
        const params: FormatParams = {a: '1234', b: 1234};
        returnFormat.setResult(params);
        const data = returnFormat.end();
        console.log(data)
        strictEqual(JSON.stringify(data), JSON.stringify({code: 0, msg: '', rs: params}))
    });
})
