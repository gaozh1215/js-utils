import {bignumber} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';

describe('math', () => {
    it('plus', function () {
        const data = bignumber.plus('1', 1);
        strictEqual(data, 2);
    });
})
