import {redux} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';


describe('redux', () => {
    it('create', () => {
        const name = 'a';
        const params = 123;
        redux.create<Number>(name, params, true);
        console.log(redux.all());
        const name1 = 'b';
        const params1 = '123';
        redux.create<String>(name1, params1);
        const data = redux.get<Number>(name);
        strictEqual(data, params);
    });
    it('listener', () => {
        const name = 'a';
        redux.listener(name, (params) => {
            console.log('listener', params)
        })
    });
    it('edit', () => {
        const name = 'a';
        const params = '098';
        redux.update<String>(name, params);
        const name1 = 'b';
        const params1 = 98;
        redux.update<Number>(name1, params1);
        const data = redux.get(name);
        strictEqual(data, params);
    });
    it('removeAndInit', () => {
        const name = 'a';
        const params = 123;
        redux.removeAndInit(name);
        const data = redux.get(name);
        strictEqual(data, params);
    });
    it('remove', () => {
        const name = 'a';
        const params = undefined;
        redux.remove(name);
        const data = redux.get(name);
        strictEqual(data, params);
    });
    it('all', () => {
        const data = redux.all();
        console.log(data);
    });
})
