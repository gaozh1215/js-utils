import {Logger} from '../src';
import {describe, it} from "mocha";
import {strictEqual} from 'assert';

describe('test_logger', () => {
    it('debug', function () {
        strictEqual(Logger.type.debug, 'debug');
    });
    it('release', function () {
        strictEqual(Logger.type.release, 'release');
    });
    it('Logger.default', function () {
        const logger = new Logger();
        logger.log('log');
        logger.warn('warn');
        logger.err('err');
    });
    it('Logger.debug', function () {
        const logger = new Logger('debug', Logger.type.debug);
        logger.log('log');
        logger.warn('warn');
        logger.err('err');
    });
    it('Logger.release', function () {
        const logger = new Logger('release', Logger.type.release);
        logger.log('log');
        logger.warn('warn');
        logger.err('err');
    });
    it('time', function () {
        // const logger = new Logger('', Logger.type.debug);
        // logger.timeStart('time');
        // let i = 1;
        // const a = setInterval(() => {
        //     logger.timeLog('time', i);
        //     i++;
        // }, 100)
        // setTimeout(() => {
        //     setTimeout(() => {
        //         clearInterval(a);
        //     }, 100)
        //     logger.timeEnd('time');
        // }, 1000)
    });
    it('init', function () {
        Logger.init(Logger.type.release);
        const logger = new Logger();
        logger.log('log');
        logger.warn('warn');
        logger.err('err');
    });
})
